# Container101
#dev1
- Introduction to Container World

## Docker

- docker sample command

### hello-java

docker build -f .dockerconfig/java.Dockerfile -t <image_name:tag> .
docker run -it --rm --name <container_name> <image_name:tag>

### hello-tomcat

docker build -f .dockerconfig/tomcat.Dockerfile -t <image_name:tag> .
docker run -d -p <local_port:container_port> --name <container_name> <image_name:tag>

### docker cammand

docker ps -a
docker images

### docker rm

docker rm -f <container_name>
docker rmi -f <image_id>
docker system prune -a

### docker start

docker start <container_name>

### docker stop

docker start <container_name>

### docker restart

docker restart <container_name>

### Note

default tag: latest

### Docker Compose

- docker-compose sample command

### docker-compose command

cd .dockerconfig
docker-compose up -d
docker-compose -f .dockerconfig/docker-compose.yml up  -d

## Kubernetes

- kubectl sample command

### kubectl apply

kubectl apply -f .kubeconfig/

### kubectl get

kubectl get namespaces
kubectl get deploy -n <name_space>
kubectl get svc -n <name_space>

### kubectl describe

kubectl descibe namespaces <name_space>
kubectl descibe deploy/<deployment_name> -n <name_space>
kubectl descibe svc/<pods_name> -n <name_space>
kubectl port-forward svc/<deployment_name> <local_port:svc_port> -n <name_space>

### kubectl delete

kubectl delete -f .kubeconfig/
